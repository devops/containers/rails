#!/bin/bash
##
## Entrypoint
##

set -x

# Just in case ...
rm -f ./tmp/pids/server.pid

initdb() {
    # Create db schema file if missing in test or development.
    # (This shouldn't happen with an established project.)
    if [[ ! -f ./db/schema.rb && $RAILS_ENV != "production" ]]; then
	echo "DB schema file not found - creating ..."
	bundle exec rake db:migrate
    fi

    case $RAILS_ENV in
	test)
	    # Always re-create database in test
	    bundle exec rake db:reset
	    ;;
	development)
	    if bundle exec rake db:migrate:status; then
		# Schema migrations table exists, so run migrations
		bundle exec rake db:migrate
	    else
		# Schema migrations table does not exist - need to run setup
		bundle exec rake db:setup
	    fi
	    ;;
	production)
	    # Production db is "protected", so db:create is not destructive
	    bundle exec rake db:create 2>/dev/null
	    # Likewise, db:schema:load will not destroy
	    bundle exec rake db:schema:load db:seed 2>/dev/null
	    # Always run migrations
	    bundle exec rake db:migrate
	    ;;
    esac
}

startup_tasks() {
    echo "===> Running startup tasks ..."
    # Run each task separately to allow failure
    for task in ${STARTUP_RAKE_TASKS[@]}; do
	bundle exec rake "${task}"
    done
}

[[ -n $STARTUP_INITDB ]] && initdb
[[ -n $STARTUP_RAKE_TASKS ]] && startup_tasks

if [[ "$(id -u)" == "0" ]]; then
    set -- gosu $APP_USER "$@"
fi

exec "$@"
