#!/bin/bash

export test_tag test_container

docker-compose up -d

code=1
result="FAILURE"
SECONDS=0
while [[ ${SECONDS} -lt 60 ]]; do
    echo -n "---> Checking health ... "
    status="$(docker inspect -f '{{.State.Health.Status}}' ${test_container})"
    echo "${status}"
    if [[ "${status}" == "healthy" ]]; then
	code=0
	result="SUCCESS"
	break
    else
	sleep 5
    fi
done

echo "${result}"

docker-compose down

exit $code
